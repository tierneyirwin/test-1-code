class Vector {
	
	private:
		double val;
		double *values;
		int numValues;
	public:
		Vector();
		Vector(int n);
		~Vector();
		void set(int idx, double val);
		double get(int idx) const;
		int size() const;
};
