#include "vector.h"

#define NULL 0

Vector::Vector() {
	numValues = 0;
	values = new double[1];	
}

Vector::Vector(int n){
	numValues = n;
	values = new double[n];
}

void Vector::set(int idx, double val){
	if(idx < 0){

	}else if(idx >= numValues){
		int newNum = idx+1;
		if(values == NULL){
			if(values) delete[] values;
			values = new double[idx+1];
			values[idx] = val;
		}else{
			double *valuesNew = new double[idx+1];
			for(int i = 0; i< numValues; i++){
				valuesNew[i] = values[i];
			}
			if(values) delete[] values;
			values = valuesNew;
			values[idx] = val;
			numValues = newNum;
		}
	}else{
		values[idx] = val;	
	}
}

double Vector::get(int idx) const {
	if(idx < 0 || idx >= numValues) {
		return NULL;
	}
	return values[idx];
}  

int Vector::size() const {
	return numValues;
}
Vector::~Vector() {
	if (values) delete[] values;
}
