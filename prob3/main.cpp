#include <iostream>
using namespace std;

#include "vector.h"

int main()
{
    Vector v;
    v.set(3, 10.7);
    v.set(4, 11.7);
    cout << "v.get(3) == " << v.get(3) << endl;
    cout << "v.get(4) == " << v.get(4) << endl;
    cout << "v.size() should be >= 5: " << v.size() << endl;

    Vector *pv = new Vector(20);
    pv->set(0, 88.8);
    pv->set(19, 99.9);
    cout << "pv->get(0) == " << pv->get(0) << endl;
    cout << "pv->get(19) == " << pv->get(19) << endl;
    cout << "pv->size() should be >= 20: " << pv->size() << endl;
    delete pv;

    Vector *pv2 = new Vector;
    for(int i = 0; i < 10; i++) {
        pv2->set(i, 2.2);
    }
    for(int i = 0; i < 10; i++) {
        cout << "pv2->get(" << i << ") == " << pv2->get(i) << endl;
    }
    delete pv2;
}
