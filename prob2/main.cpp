#include <iostream>
using namespace std;

#include "tree.h"

int main()
{
    Tree *mytree = new Tree('a');
    cout << "Tree depth = " << mytree->maxDepth() << endl;
    cout << "Number of Nodes = " << mytree->totalNodes() << endl;
    cout << "Num of children: " << mytree->getNumChildren() << endl;
    Tree *child1 = new Tree('b');
    cout << "Tree depth = " << mytree->maxDepth() << endl;
    cout << "Number of Nodes = " << mytree->totalNodes() << endl;
    mytree->addChild(child1); // now mytree has *responsibility* for the memory of child1
    cout << "Num of children: " << mytree->getNumChildren() << endl;
    mytree->addChild(new Tree('c'));
    mytree->addChild(new Tree('d'));
    mytree->addChild(new Tree('e'));
    cout << "Tree depth = " << mytree->maxDepth() << endl;
    cout << "Number of Nodes = " << mytree->totalNodes() << endl;
    cout << "Num of children: " << mytree->getNumChildren() << endl;
    cout << "Value at pos 2: " << mytree->getChild(2)->getVal() << endl;
    delete mytree; // will cause the destructor function to execute
    

	Tree *testtree = new Tree('s');
	cout << "Tree depth = " << testtree->maxDepth() << endl; // 1
	cout << "Number of Nodes = " << testtree->totalNodes() << endl; //1
	testtree->addChild(new Tree('t'));
	testtree->addChild(new Tree('u'));
	testtree->addChild(new Tree('v'));
	Tree *child2 = new Tree('k');
	child2->addChild(new Tree('l'));
	child2->addChild(new Tree('f'));
	child2->addChild(new Tree('i'));
	testtree->addChild(child2);
        cout << "Tree depth = " << testtree->maxDepth() << endl;//3
        cout << "Number of Nodes = " << testtree->totalNodes() << endl;//8 
	delete testtree;
	return 0;
}


