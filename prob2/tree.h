class Tree {

    private:
        char c;
        Tree **children;
        int numChildren;

    public:
        Tree(char c);
        ~Tree(); // destructor
        int getNumChildren() const;
        bool addChild(Tree *child);
        //bool removeChild(int idx);
        //bool removeChild(Tree *child);
        Tree *getChild(int idx) const;
        //bool hasChild(Tree *child) const;
        char getVal() const;
        bool hasVal(char c) const;
        char findMax() const;
	int maxDepth() const;
	int totalNodes() const;
};



