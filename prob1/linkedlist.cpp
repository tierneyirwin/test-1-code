#include <iostream>
#include <cassert>
using namespace std;

#define NULL 0
#include "tree.h"
#include "linkedlist.h"

Node::Node(void* _val) {
    val = _val;
    next = NULL;
}

void Node::setNext(Node* _next) {
    next = _next;
}

Node* Node::getNext() {
    return next;
}

void* Node::getVal() {
    return val;
}

Tree::Tree(char c) {
    this->c = c;
    numChildren = 0;
    children = NULL;
}

void Tree::addChild(Tree *child) {
	Tree **childrenNew = new Tree*[++numChildren];
	// keep ++i. you are not expected to understand this.
        for(int i = 0; i < numChildren-1; ++i) {
		childrenNew[i] = children[i];
        }
       	childrenNew[numChildren-1] = child; // save the star child into the children array
        if(children) delete[] children;
            	children = childrenNew;
}
   
Tree *Tree::getChild(int idx) const {
    	if(idx < 0 || idx >= numChildren) return NULL;
        return children[idx];
}
   
char Tree::getVal() const {
	 return c;
}

Tree::~Tree() {
	for(int i = 0; i < numChildren; i++) {
        	delete children[i];
        }
        if(children) delete[] children;
}

LinkedList::LinkedList() {
    head = tail = NULL;
    len = 0;
}
LinkedList::~LinkedList() {
	Node* deleted = head;
	while(deleted!= NULL){
		Node* next = deleted->getNext();
		delete deleted;
		deleted = next;
	}	
}

void LinkedList::add(void* val) {
    Node* n = new Node(val);
    len++;
    if(head == NULL){ head = n;}
    if(tail != NULL){ tail->setNext(n);}
    tail = n;
}

void LinkedList::remove(int idx) {
    if(idx > len) { return; }
    Node *n = head;
    Node *pn;
    while(idx > 0) {
        pn = n;
        n = n->getNext();
        idx--;
    }
    pn->setNext(n->getNext());
    delete n;
    len--;
}

void LinkedList::valAt(int idx, int type) {
    assert(idx <= len && idx >= 0);
    Node *n = head;
    while(idx > 0) {
        n = n->getNext();
        idx--;
    }
    if(type == 1){//int type
   	 cout << *(int*)(n->getVal()) << endl;
    }else if(type == 2){//double
	cout << *(double*)(n->getVal()) <<endl;	
    }else if(type == 3){ //tree
	cout <<(*(Tree**)(n->getVal()))->getVal() << endl;
    }else if( type == 4){ //char
	cout << *(char*)(n->getVal()) << endl;
    }else if(type == 5){ //float
	cout << *(float*)(n->getVal()) <<endl;
    }else{
	cout << "Valid type numbers include: 1-int, 2-double, 3-tree, 4-char, 5-float" << endl;
    }
}

int LinkedList::length() {
    return len;
}

