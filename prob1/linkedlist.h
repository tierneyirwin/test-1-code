


class Node {
    public:
        Node(void* _val);
        void setNext(Node* _next);
        Node* getNext();
        void* getVal();

    private:
        void* val;
        Node* next;
};

class LinkedList {

    public:

        LinkedList();
	~LinkedList();
        void add(void* val);
        void remove(int idx);
        void valAt(int idx, int type);
        int length();

    private:

        Node* head;
        Node* tail;
        int len;

};


