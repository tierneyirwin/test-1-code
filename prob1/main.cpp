
#include <iostream>
using namespace std;
#include "tree.h"
#include "linkedlist.h"

int main()
{
    LinkedList *list = new LinkedList();
    cout << "Length: " << list->length() << endl;
    int n = 52;
    void* pn = &n;
    list->add(pn);
    cout << "Length: " << list->length() << endl;
    cout << "Val at 0: ";
    list->valAt(0,1);
    delete list;

	cout << "Test cases"<<endl;
	LinkedList *list2 = new LinkedList();
	int f =5;
	void* pf = &f;
	list2->add(pf);
	double w = 4.33;
	void* pw = &w;
	list2->add(pw);
	double x = 2.4;
	void* px = &x;
	list2->add(px);
	Tree *mytree = new Tree('r');
	Tree *child = new Tree('c');
	mytree->addChild(child);
	void* pmytree = &mytree;
	list2->add(pmytree);
	cout <<"Length: " << list2->length() <<endl;
	cout << "Val at 0: ";
	list2->valAt(0,1);
	cout << "Val at 1: ";
	list2->valAt(1,2);
	cout << "Val at 2: ";
	list2->valAt(2,2);
	cout << "Val at 3: ";
	list2->valAt(3,3);
	delete list2;
	delete mytree;	
    return 0;
}


