class Tree {

    private:
        char c;
        Tree **children;
        int numChildren;

    public:
        Tree(char c);
        ~Tree(); // destructor
        void addChild(Tree *child);
        Tree *getChild(int idx) const;
        char getVal() const;
};



